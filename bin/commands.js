const { program } = require('commander');
const pkg = require('../package.json');

program
  .command('A', 'Open the last current note for appending');
  .parse(process.argv)
