#!/usr/bin/env node

/////***********  CODE-START  ***********/////

//const os = require('os');
const fs = require('fs');
const path = require('path');
const cp = require('child_process');
const inquirer = require('inquirer');
const sh = require('shelljs');
const colors = require('colors');
const PromptQue = require('../lib/promptque');
const { Crypta } = require('../lib/crypta');
const program = require('commander');
const pkg = require('../package.json');
const usrHomeDir = require('os').homedir();
const capturaDir = path.join(usrHomeDir, 'Captura-Notes/');
const settingsDir = path.join(capturaDir, '.settings/');
const settingsFile = path.join(settingsDir, 'settings.json');
const editor = 'vi';
const EDITOR = process.env.EDITOR || editor;

const config = require(settingsFile);

// <<<<<<<<< WORK HERE (below) <<<<<<<<<<<<<<<<
// const config = undefined;
//
// try {
//   config = require(settingsFile);
// } catch (e) {
//   console.log(`${e}`.gray);
// } finally {
//   console.log('\nCreating New Settings File Now!')
//   fs.writeFileSync(settingsFile, '');
// }

// <<<<<<<<< WORK HERE (above) <<<<<<<<<<<<<<<<

// ***  FUNCTION DEFINITIONS  *** //

// A Generic DEBUG Function
function testFunc(str) {
  console.log(`Option Selected: ${str}`);
}

// Function to generate a new note/notebook file
function CreateNote(fn, contentstr) {
  fs.appendFile(fn, contentstr, (err) => {
    if(err) {
      throw err;
    } else {
      OpenEditorNewEntry(fn);
    }
    // console.log(`${fn} created successfully`.yellow);
  });
}

// Function to enable the creation of a new Note/Journal Entry Object (this actually opens the user's choice text editor)
function OpenEditorNewEntry(fn) {
  // const terminalcmd = `${EDITOR} ${fn}`;

  cp.spawnSync(EDITOR, [`${fn}`], {
    detached: true,
    shell: true,
    stdio: 'inherit'
  });

  process.exit();
}

// testArgsArrayLength() function below tests to see if no commands arguments were passed in to 'process.argv' and if this is so upon the user starting the application with no command args, then the app will run as if the '+' command argument was provided, thus enabling the app to have a default command when user only enters 'captura' without a command argument provided
function testArgsArrayLength(args) {
  if(args.length === 2) {
    process.argv.push('+');
  }
}

//** Function to append extra info to user Help option
function addHelpInfo(infotext) {
  if(infotext !== '') {
    if(process.argv[2] === '-h' || process.argv[2] === '--help') console.log(infotext);
  }
}

// Function for constructing a 'Filename/Pathname'
function buildFileName(dirPath, notebookname) {
    let res = '';
    let fn = notebookname.toLowerCase() + '-cn.';
    fn += datetimestr+'.md';
    res = path.join(dirPath, fn);
    return res;
}

// Function to build a Title for the Document & will include the assigned Notebook name and a Timestamp
function buildDocTitle(notebookname) {
    let doctitle = `# Captura Note File: ${notebookname.toLowerCase()}-cn.`;
    doctitle += datetimestr+'.md  \n';
    return doctitle;
}

// Function to construct a note entry title
function buildEntryTitle(notebookname) {
    let entryTitle = '\n## Entry ';
    entryTitle += datetimestr + ` [${notebookname.toLowerCase()}] >>>\n`;
    return entryTitle;
}

// Function to build a Date String
function buildDateStr(format) {
    // 'format' arg should be 'file' or 'entry'
    let dtStr = '';
    switch(format) {
        case 'file': {
            let ts = Date.now();
            let date = new Date();
            let days = ('0'+date.getDate()).slice(-2);
            let month = ('0'+(date.getMonth()+1)).slice(-2);
            let year = date.getFullYear();
            dtStr = `${year}-${month}-${days}.ts${ts}`;
            break;
        }
        case 'entry': {
            let ts = Date.now();
            dtStr = `${Date(ts).slice(0,24)} (ts:${ts})`;
            break;
        }
    }
    return dtStr;
}

// Function to create a Captura-Notes user home directory
// !!> This needs t'be rewritten [sloppy code] ..tsk ..tsk
function makeCapturaDir(dirPath) {
  // First Check if Dir exists
  let dirExists = fs.existsSync(dirPath);

  // If Dir not exists create the Dir
  if(!dirExists) {
      dirExists = makeDir(capturaDir, false);
  }

  // Second Check if Dir exists after try to create Dir
  if(!dirExists) { // Error Handler If Notes Dir couldn't be created
      console.log(`Error: Captura-Notes Directory Could Not Be Created! - ErrorCode: 911`.brightRed);
      process.exit(911);
  }
}

// Function to make a hidden(linux) dot ,settings directory that will store type json settings docs
function makeSettingsDir(dirPath) {
  // First Check if Dir exists
  let dirExists = fs.existsSync(dirPath);
  // If Dir not exists create the Dir
  if(!dirExists) {
      dirExists = makeDir(settingsDir, false);
      if(dirExists) {
// LAST:
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        updateSettingsFile(settingsObj);
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      } else {
        console.error(`Error: Settings Directory could not be created. checkout error-code <ec:666> Terminating (Main Process)...!\n`.brightRed);

        process.exit();
      }
  }
  // // Second Check if Dir exists after try to create Dir
  // if(!dirExists) { // Error Handler If Notes Dir couldn't be created
  //     console.log(`Error: Captura-Notes Directory Could Not Be Created! - ErrorCode: 911`.red);
  //     process.exit(911);
  // }
}

// Function to create a new directory
function makeDir(dirPath, recurse) {
    let res = false;
    fs.mkdirSync(dirPath, { recursive: recurse }, (error) => {
        if(error) {
            console.error(`Error: ${error}`.red);
            res = false;
        }
    });
    if(fs.existsSync(dirPath)) {
        res = true;
    } else {
        res = false;
    }
    return res;
}

// Function to generate a basic file
function makeFile(filePath, fileContent='') {
    let res = false;
    fs.writeFile(filePath, fileContent, (error) => {
        if(error) {
            console.error(`Error: ${error}`.red);
            res = false;
        }
    });
    if(fs.existsSync(filePath)) {
        res = true;
    } else {
        res = false;
    }
    return res;
}

// Function to generically prompt and retreive user input
function que(question, callback) {
  let stdin = process.stdin,
      stdout = process.stdout;

  stdin.resume();
  stdout.write(question);

  stdin.once('data', (data) => {
        callback(data.toString().trim());
  });
}

// Function to construct a note entry title
function buildEntryTitle(notebookname) {
    let entryTitle = '\n## Entry ';
    entryTitle += datetimestr + ` [${notebookname.toLowerCase()}] >>>\n`;
    return entryTitle;
}

// <<<<<<<<<<<<<<<<< HERE (below) <<<<<<<<<<<<<<<<<<<<<
function ReadSingleFile(fd) {


  // fs.readFileSync(fd, 'utf-8', (err, data) => {
  //   if(err) throw err;
  //
  //   console.log(data);
  // });

  // console.log(fd);
  process.exit();

  // fs.readFileSync(fd, (err, data) => {
  //   if(err) throw err;
  //
  //   let contents = data.toString();
  //   return JSON.parse(contents);
  //   // console.log(contents);
  // })
// <<<<<<<<<<<<<<<<< HERE (above) <<<<<<<<<<<<<<<<<<<<<

  // let file = fd.target.files[0];
  // if(!file) {
  //   return;
  // }
  // let reader = new FileReader();
  // reader.onload = function(fd) {
  //   let contents = fd.target.result;
  //   if(contents !== '' || contents !== undefined) {
  //     // convert a json object to javascript object and return
  //     return JSON.parse(contents);
  //   }
  // };
}

function updateSettingsFile(settingsDataObj) {
  newSettingsDataObj = JSON.stringify(settingsDataObj);
  fs.writeFile(settingsFile, newSettingsDataObj, (err) => {
    if(err) throw err;
  });
}

// function updateSettingsFile(settingsDataObj) {
//   settingsDataObj = JSON.stringify(settingsDataObj);
//   fs.writeFile(settingsFile, settingsDataObj, (err) => {
//     if(err) throw err;
//   });
// }

// Just *another Init() function **hehe he ..hee
function Init() {
  makeCapturaDir(capturaDir);
  makeSettingsDir(settingsDir);
  testArgsArrayLength(process.argv);
}

// Function to perform any final initializing Captura-Notes CLI at runtime-startup; !NOTE***> Eventually this next function block of code needs to be re-written and combine with the 'Init()' function below !!!
function GetDocNames(dirPath, notebookname) {

    let fn = buildFileName(dirPath, notebookname); // filename
    let dt = buildDocTitle(notebookname); // doctitle
    let et = buildEntryTitle(notebookname); // entrytitle

    return [fn, dt, et];
}

//----------------------------------------------------------
// EXECUTION OF CODE:
//-------------------------------------------------------
// *** Initialization Variables *** //

let settingsObj = {
  'LastModifiedNote': 'general', //<Note File Name>
  'NoteBooksList': ['general'], //<a list of notebook names>
  'LastNotebookUsed': 'general',
  'UsersCustomSignature': undefined
};
let settingsFileContent = '';

let initDocumentNames = [];
let filename = '';
let doctitle = '';
let entrytitle = '';
let notebook = 'General';
let datetimestr = buildDateStr('file');
const help = `\nWelcome to Captura Notes! For Help enter 'captura -h' at the terminal prompt below. Quickly capture your thought!\n`.brightBlue;
// const help = `\nWelcome to Captura Notes. For Help, start Captura Notes by passing the '-h' or '--help' option to view help info and command reference info. For example type 'captura -h' in at the terminal prompt. To start a new note simply type 'captura'\n`.brightBlue;
const helpinfo = ''; // used for adding extra help info to when the -h (help command) is envoked.

// Blurp out a 'sticky' Help
console.log(help);

// call to main Init()
Init();
// testArgsArrayLength(process.argv);

program
  .version(pkg.version);

program
  .command('+')
  .description('Create new note')
  .action(
    () => {
      que('Enter a notebook name or press the \'Enter Key\' if note is to be filed under your default notebook:'.brightGreen + ' >> \n'.cyan,
      (input) => {

        if (input === '') {
          notebook = 'General';
        } else {
          notebook = input;
        }

        [ filename, doctitle, entrytitle ] = GetDocNames(capturaDir, notebook);

        // let sdata = GetDocNames(capturaDir, notebook);
        // console.log(sdata);
        // console.log(`filename > ${filename}\ndoctitle > ${doctitle}\nentrytitle > ${entrytitle}`.white);

        let content = doctitle + entrytitle;

        CreateNote(filename, content);

        settingsObj['LastModifiedNote'] = filename;
        settingsObj['NoteBooksList'].push(notebook.toLowerCase());
        settingsObj['LastNotebookUsed'] = notebook.toLowerCase();
        updateSettingsFile(settingsObj);

        // // process.exit();
      });
    }
  );
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  program             // WORK HERE //
    .command('a')
    .description('Append to last modified note')
    .action(() => {
      let lastnote = config['LastModifiedNote'];
      let lastnotebook = config['LastNotebookUsed'];
      entrytitle = buildEntryTitle(lastnotebook);
      CreateNote(lastnote, `${entrytitle}\n`);
  });

  program
    .command('o')
    .description('Open an existing notebook and append new note')
    .action(() => {
      console.log(`Captain, leaving bridge!`);
      process.exit();
    });

  program
    .command('x')
    .description('Append to last modified note without new Date-time entry heading')
    .action();
// <<<<<<<<<<<<<<   HERE (below)   <<<<<<<<<<<<<<<<<<<
  program
    .command('nb')
    .description('List all notebooks')
    .action(() => {
      let { NoteBooksList } = config;
      // console.log(NoteBooksList);
      NoteBooksList.forEach(item => {
        console.log(item)
      });
    });
// <<<<<<<<<<<<<<   HERE (below)   <<<<<<<<<<<<<<<<<<<

  program
    .command('l')
    .description('List all notebooks and note titles')
    .action();

  program
    .command('d')
    .description('Delete a notebook')
    .action();

  program
    .command('e')
    .description('Encrypt a notebook')
    .action();

  // *** NOTE!! command 't' below is only for dev testing and should not be in published release.
  program
    .command('t')
    .description('Only For Dev Testing!')
    .action(() => {
      console.log(capturaDir);
    });

addHelpInfo(helpinfo);

program.parse(process.argv);

//DEBUG: console.log(`${process.argv}`.grey);

// DEV: Unused

// process.exit();

//function runOSCode(ostype, func, args) {
//    switch(osType) {
//        case 'linux': {
//            let res = func(args);
//            if(res) {
//                return res;
//            }
//            break;
//        }
//        case 'win32': { let res = func(args);
//            if(res) {
//                return res;
//            }
//            break;
//        }
//        case 'darwin': {
//            let res = func(args);
//            if(res) {
//                return res;
//            }
//            break;
//        }
//    }
//}
