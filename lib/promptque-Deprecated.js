/*//// Module Introspection Stuff - Begin ////*/
const __MODULEINFO__ = {
    package: 'lib',
    name: 'promptque',
    sub: 'promptque class definition',
    context: 'Promptque is a CLI class module for creating command-line interface menu prompt queries that utilize Inquirer JS and Chalk JS Javascript Modules. Note: Requires Inquirer.js and Chalk.js npm modules',
    version: '1.0.0',
    dependencies: ['inquirer.js','shell.js','colors.js'],
    author: 'Donovan Edwards',
    author_email: 'robogenus@gmail.com',
    repository: undefined,
    license: 'MIT',
    banner: `\n
    -----------------------------
    :      {1001}^ -,           :
    :     _________  :  ___     :
    :    |       _=_ |     |    :
    :    |     K[*_^]3 (]  |    :
    :    | #    <=.=>  /   |    :
    :    | W)__/]888[\\/    |    :
    :    |     <| + |>     |    :
    :    |     ((<?>))     |    :
    :    |   |.]-DCE-[.|   |    :
    :    |   +[o]---[o]+   |    :
    :    |   +:::+ +:::+   |    :
    :    |  {[RoBoGeNus]]} |    :
    :    -------------------    :
    :                 © 2019    :
    -----------------------------
    `
}
exports.MODULEINFO = __MODULEINFO__;
/*//// Module Introspection Stuff - End ////*/


/////***********  CODE-START  ***********/////


/* See Examples Below To Test This Class */

const inquirer = require('inquirer');
const sh = require('shelljs');
const colors = require('../colors');

// class to instantiate an inquirer prompt
class PromptQue {
  constructor(pP) { // pP is an passed Object of params {key:value pairs}
    this.pP = pP;
    this.answer = undefined; // a class property to hold user's answer to prompt
    this.clearTerm = false; // set true if need to clear user window after prompt
    //this.promptColor = color.tan; // assign a color to prompt question text from colors.js
    this.statusBar = new inquirer.ui.BottomBar();
    this.doStatusMsg = false; // set true if need to display status message
    this.statusColor = color.warning; // assign a color to status message text from colors.js
    this.statusMsg = undefined; // assign a status message
    // this.choicesColor = color.offwhite;

    // the switch below tests whether a normal Array
    // or an Assoc Array Object was passed as answer 'choices'
    switch(pP.hasOwnProperty('choices')) {
      case true:
        let testIfArray = Array.isArray(this.pP.choices);
        if(!testIfArray) { // if not Array convert 'choices' object to array
          this.pP.choices = this.GenArr(pP.choices);
        }
      case false:
        break;
    }

    this.updateParams(); // call to set colors/styles on color enabled params
  }

// ### !!! -----------------  DEBUG THIS!  ------------------------------ >>>

// ### !!! Prompt() Method: Can't Get The Answer Returned !! ------ ###
  // call Prompt() method to run the PromptQue instance
  Prompt() {
    return new Promise((resolve, reject) => {
      // doStatusMsg is set true, display status message
      this.checkStatusMsg();
      inquirer.prompt([
        this.pP // this is the Object Params passed to constructor
      ]).then((answer) => { // getting user's answer to prompt
        this.answer = answer[this.pP.name];
        return this.answer;
      }).catch((err) => {console.log(err)});
    })
    .then(() => this.clearStatus())
    .then(() => this.clear(this.clearTerm)); // if true : clear user window after prompt
  }

// ### !!! -----------------  DEBUG THIS!  ------------------------------ >>>

  showData() {
    let keys = Object.keys(this.pP);
    keys.forEach((key) => {
      if(key === 'choices') {
        let arr = this.pP['choices'];
        let choicesStr = '';
        let cntr = 0;
        arr.forEach((val) => {
            if(cntr < this.pP['choices'].length) {
              choicesStr += `${val}, `;
            } else {
              choicesStr += `${val}`;
            }
            cntr++;
        });
        console.log(color.gray(`choices: ${choicesStr}`));
      } else {
        console.log(color.gray(`${key}: ${this.pP[key]}`));
      }
    });
    console.log(color.gray(`clearTerm: ${this.clearTerm}`));
    //let C = this.promptColor('#COLOR');
    //console.log(color.gray(`promptColor: ${C}`));
    console.log(color.gray(`statusMsg: ${this.statusMsg}`));
    console.log(color.gray(`doStatusMsg ${this.doStatusMsg}`));
    C = this.statusColor('#COLOR');
    console.log(color.gray(`statusColor: ${C}`));
  }

  // function to convert object into regular array
  GenArr(arr) {
    let regArr = [];
    for(var key in arr) {
      var value = arr[key];
      regArr.push(value);
    }
    return regArr;
  }

  // rebuilds colored enabled params with specified colors/styles
  updateParams() {
    let keys = Object.keys(this.pP);
    keys.forEach((key) => {
      switch (key) {
        case 'message':
          this.pP['message'] = this.promptColor(this.pP['message']);
          break;
      }
    });

    if(this.statusMsg) {
      this.statusMsg = this.statusColor(this.statusMsg);
    }
  }

  // displays a new message (msg) in the Status Bar
  checkStatusMsg() {
    if(this.doStatusMsg) {
      let msg = `${this.statusColor.bold(this.statusMsg)}\n`;
      this.setStatus(msg);
    }
  }

  // Set a status message for the prompt
  setStatus(msg) {
    this.statusBar.updateBottomBar(msg);
    // return msg;
  }

  // Clears the Status Bar's Field
  clearStatus() {this.statusBar.updateBottomBar('')}

  // Method to get user's selected choice for this prompt
  getAnswer() {
    return this.answer;
  }

  // console.log the user's selected choice for this prompt, mainly for debugging
  logAnswer() {
    if(this.answer !== undefined) {
      console.log(color.ltgray.italic.bold(`User Choice: ${this.answer}`));
    }
  }

  // function to clear user terminal screen
  clear(bool) {
    if(bool) {
      sh.exec('clear');
    }
  }
}

// // Exporting The PromptQue Class as 'PromptQue'
exports.PromptQue = PromptQue;
// exports.color = color;

/////***********  CODE-END  ***********/////

//----------------------------------------------------
/*
const sizeList = {0:'Jumbo',1:'Large',2:'Standard',3:'Medium',4:'Small',5:'Micro'};
// create an object to pass to new instance
let cPromptData = {
  type: 'list',
  message: color.tan('What size Pizza do you need?'),
  name: 'pizza',
  choices: sizeList
};
let cPrompt = new PromptQue(cPromptData);
cPrompt.statusMsg = 'Pizza! I Love Pizza!';
cPrompt.doStatusMsg = true;
cPrompt.Prompt();
*/
//-----------------------------------------------------

// const myPromise = new Promise((resolve,reject) => {
//   cPrompt.Prompt();
// }).then(() => console.log(`Selected: ${cPrompt.answer}`));
// myPromise.then(() => console.log(`Selected: ${cPrompt.answer}`));

// let ans = cPrompt.Prompt();
// console.log(`Selected: ${ans}`);


/* *****  USAGE NOTES  ***** /*
+-----

// --------  Example Instantiation  -------- //
// sizeList can be either an Assoc Array Object or Regular Array
// const sizeList = ['Jumbo','Large','Standard','Medium','Small','Micro'];
const sizeList = {0:'Jumbo',1:'Large',2:'Standard',3:'Medium',4:'Small',5:'Micro'};
// create an object to pass to new instance
let cPromptData = {
  type: 'list',
  message: color.tan('What size Pizza do you need?'),
  name: 'pizza',
  choices: sizeList
};
let cPrompt = new PromptQue(cPromptData);
cPrompt.statusMsg = 'Pizza! I Love Pizza!';
cPrompt.doStatusMsg = true;
cPrompt.Prompt();
// cPrompt.showData();

;-----


/* *****  DEV SAMPLE CODE  ***** /*
+-----
Put sample-test code below here then move
to ./devnotes/dev-test-code.<number>.txt
as needed.
+-----



*/
