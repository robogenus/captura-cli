
/*//// Module Introspection Stuff - Begin ////*/
const __MODULEINFO__ = {
    package: 'lib',
    name: 'colors',
    sub: 'cli colors & styles',
    context: 'Colors & Styles module that utilizes Colors JS',
    version: '1.0.0',
    dependencies: ['colors.js'],
    author: 'Donovan Edwards',
    author_email: 'robogenus@gmail.com',
    repository: undefined,
    license: undefined,
    banner: `\n
    -----------------------------
    :      {1001}^ -,           :
    :     _________  :  ___     :
    :    |       _=_ |     |    :
    :    |     K[*_^]3 (]  |    :
    :    | #    <=.=>  /   |    :
    :    | W)__/]888[\\/    |    :
    :    |     <| + |>     |    :
    :    |     ((<?>))     |    :
    :    |   |.]-DCE-[.|   |    :
    :    |   +[o]---[o]+   |    :
    :    |   +:::+ +:::+   |    :
    :    |  {[RoBoGeNus]]} |    :
    :    -------------------    :
    :                 © 2019    :
    -----------------------------
    `
}
exports.MODULEINFO = __MODULEINFO__;
/*//// Module Introspection Stuff - End ////*/


/////***********  CODE-START  ***********/////

const colors = require('colors');


exports.color = color;


/////***********  CODE-END  ***********/////

/* *****  DEV NOTES  ***** /*
+-----
Put temp dev-notes below here then move
to ./devnotes/'dev-notes.<number>.txt'
as needed.
+-----

NEEDS MORE WORK!

add any of these colors/styles or add custom ones:

// turqoise
turq: chalk.hex('#02B388');
bgturq: chalk.whiteBright.bgHex('#00624F');
// info: blue
info: chalk.hex('#0092ff');
bginfo: chalk.white.bgHex('#285095');
// warning: orange
warn: chalk.hex('#ff8700');
bgwarn: chalk.whiteBright.bgHex('#ce6d00');
// error: red
error: chalk.hex('#ff0000');
bgerror: chalk.whiteBright.bgHex('#eb0000');
// dim: grey
grey: chalk.hex('#a1a1a1');
bggrey: chalk.hex('#a1a1a1').bgHex('#2d2d2d');
// dim: tan
tan: tan = chalk.hex('#cca26c');
bgtan: chalk.hex('#cca26c').bgHex('#3b2b1c');
// yellow
yellow: chalk.hex('#fff500');
bgyellow: chalk.whiteBright.bgHex('#e6c200');
// dark
dark: chalk.hex('#000000');
bgdark: chalk.hex('#000000').bgHex('#747474');
darkbold: chalk.hex('#000000').bold;
bgdarkbold: chalk.hex('#000000').bgHex('#747474').bold;
// red bold
redbold: chalk.red.bold;
// red italic
reditalic: chalk.red.italic;
// red underline
redul: chalk.red.underline;
// red strike
redstrike: chalk.red.strikethrough;
// red dim
reddim: chalk.red.dim;
// blue
blue: chalk.blue;
// blue bold
bluebold: chalk.blue.bold;
// blue dim
bluedim: chalk.blue.dim;
// blue italic
blueitalic: chalk.blue.italic;
// green
green: chalk.green;
// green bold
greenbold: chalk.green.bold;
// green dim
greendim: chalk.green.dim;
// green italic
greenitalic: chalk.green.italic;

// TEXT-STYLES //

// bold text
bold: chalk.bold;
// dim text
dim: chalk.dim;
// italic text
italic: chalk.italic;
// underline
ul: chalk.underline;
// inverse - invert bg and fg
invert: chalk.inverse;
// strikethrough
strike: chalk.strikethrough;
// bold italic
bolditalic: chalk.bold.italic;
// bold dim
bolddim: chalk.bold.dim;
// dim italic
dimitalic: chalk.dim.italic;
// dim underline
dimul: chalk.dim.underline;
// italic underline
italicul: chalk.italic.underline;

;-----

/* *****  DEV SAMPLE CODE  ***** /*
+-----
Put sample-test code below here then move
to ./devnotes/dev-test-code.<number>.txt
as needed.
+-----

// --- Sleep ..ZZZzzzz ..ZZZzz.zz
sleep(500).then(() => {
  //zZz* Do ... {wtfe!} ... after the sleep--> *zZz//
  if(condition) {
    do ,,,
  }
});

*/
