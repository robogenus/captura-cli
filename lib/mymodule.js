
/*//// Module Introspection Stuff - Begin ////*/
const __MODULEINFO__ = {
    package: 'utils',
    name: 'codetest',
    sub: 'class-constructor-example',
    context: 'Class Constructor Example: Passing one Param Object argument containing multi-params to class constructor with default values',
    version: '1.0.0',
    author: 'Donovan Edwards',
    author_email: 'robogenus@gmail.com',
    repository: undefined,
    license: undefined,
    notes: `

    Note1: 'Example Note ...',
    Note2: {
      A: 'Another Example Note (A)...',
      B: 'one more sub Example note (B)...'
    },
    Note3: 'and So on...'

    `,
    banner: `\n
    -----------------------------
    :      {1001}^ -,           :
    :     _________  :  ___     :
    :    |       _=_ |     |    :
    :    |     K[*_^]3 (]  |    :
    :    | #    <=.=>  /   |    :
    :    | W)__/]888[\\/    |    :
    :    |     <| + |>     |    :
    :    |     ((<?>))     |    :
    :    |   |.]-DCE-[.|   |    :
    :    |   +[o]---[o]+   |    :
    :    |   +:::+ +:::+   |    :
    :    |  {[RoBoGeNus]]} |    :
    :    -------------------    :
    :                 © 2019    :
    -----------------------------
    `
}
exports.MODULEINFO = __MODULEINFO__;
/*//// Module Introspection Stuff - End ////*/


/////***********  CODE-START  ***********/////



/////***********  CODE-END  ***********/////

/* *****  DEV NOTES  ***** /*
+-----
Put temp dev-notes below here then move
to ./devnotes/'dev-notes.<number>.txt'
as needed.
+-----

`
Note1: 'Example Note ...',
Note2: {
  A: 'Another Example Note (A)...',
  B: 'one more sub Example note (B)...'
},
Note3: 'and So on...'
`;


;-----

/* *****  DEV SAMPLE CODE  ***** /*
+-----
Put sample-test code below here then move
to ./devnotes/dev-test-code.<number>.txt
as needed.
+-----

// --- Sleep ..ZZZzzzz ..ZZZzz.zz
sleep(500).then(() => {
  //zZz* Do ... {wtfe!} ... after the sleep--> *zZz//
  if(condition) {
    do ,,,
  }
});

*/
