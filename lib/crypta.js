/*//// Module Introspection Stuff - Begin ////*/
const __MODULEINFO__ = {
    package: 'lib',
    name: 'crypta js',
    sub: 'Crypta Class Definition',
    context: "Crypta is a nodejs class module that defines the Crypta class. Creating a new Crypta object will enable the encryption and decryption of Data. This Class module depends on and utilizes the NPM Module 'strong-cryptor'-v-2.2.0 which encrypts data using aes-256-cbc algorithm with base64 encoding. This Crypta Class is really just a wrapper for 'Strong-Cryptor' to be able to use a Password to Encrypt/Decrypt some Data via using the HashIt Class.",
    version: '1.0.0',
    dependencies: ['strong-cryptor^2.2.0'],
    author: 'Donovan Edwards',
    author_email: 'robogenus@gmail.com',
    repository: "git+https://robogenus@bitbucket.org/robogenus/crypta.git",
    license: "MIT",
    banner: `\n
    -----------------------------
    :      {1001}^ -,           :
    :     _________  :  ___     :
    :    |       _=_ |     |    :
    :    |     K[*_^]3  (] |    :
    :    | #    <=.=>   /  |    :
    :    | W)__/]888[\\/   |    :
    :    |     <| + |>     |    :
    :    |     ((<?>))     |    :
    :    |   |.]-DCE-[.|   |    :
    :    |   +[o]---[o]+   |    :
    :    |   +:::+ +:::+   |    :
    :    |  {[RoBoGeNus]]} |    :
    :    -------------------    :
    :                 © 2019    :
    -----------------------------
    `
}
exports.MODULEINFO = __MODULEINFO__;
/*//// Module Introspection Stuff - End ////*/

/////***********  CODE-START  ***********/////
/* See Examples Below To Test This Class */

const { Encryptor, Decryptor } = require('strong-cryptor');
const alphas = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

class Crypta {
  constructor(password) {
    this.pswrd = password; // Example Password: xN*_*JfF91zR3-!69
    this.cycles = 1; // Number of times to cycle encryption algorithm, higher number slower but more secure
    this.saltRounds = 10;
    this.key = this.makeKey(this.pswrd);
    this.encryptor = new Encryptor({ key: this.key, encryptionCount: this.cycles });
    this.decryptor = new Decryptor({ key: this.key, encryptionCount: this.cycles });
  }

  makeKey(pswrd) {
    const Hasher = new HashIt(pswrd);
    const key = Hasher.hash;
    return key;
  }

  Encrypt(data) {
    const encrypted = this.encryptor.encrypt(data);
    return encrypted;
  }

  Decrypt(encrypted) {
    const decrypted = this.decryptor.decrypt(encrypted);
    return decrypted;
  }

}

class HashIt {

  constructor(password) {
    this.password = password;
    this.exponent = 7.77; // Math.pwr() Numeric Exponent for 2nd Argument
    this.hash = this.makeHash(this.password);
  }

  makeHash(pswrd) {
    let pw = pswrd.split(''); // chunking each letter of pswrd into 'pw' Array
    pw = this.makeSum(pw); // running 'makeSum()' on pw Array and output back to 'pw'

    // running 'makeDaxar' on 'pw' and converting return to String
    let numStr = this.makeDaxar(pw, this.exponent).toString();
    numStr = numStr.replace('.','3'); // replacing '.' with '3'
    let num2xStr = parseInt(numStr, 10); // converting 'numStr' back to Integer

    // doubling value of 'numStr'(Integer) and converting back to String
    num2xStr = (num2xStr * 3).toString();
    numStr = numStr + num2xStr; // concatenating 'numStr'(Str) and 'num2xStr'(Str)

    // encoding new 'numStr'(Str) to base64 assigned to 'x'
    let x = (Buffer.from(numStr)).toString('base64');

    let xN = x.split(''); // chunking each character of 'x' as element of xN array
    xN = xN.splice(0,32); // getting 32 Chars from 'xN' re-assigning to 'xN'
    // xN var now contains the Hash Key that we need
    xN = xN.join('').toUpperCase(); // converting 'xN' array back to Uppercase String

    return xN;
  }

  makeDaxar(num, exp) {
    // processing 'num' through Daxaring Math ...
    return Math.sqrt(Math.pow(num, exp));
  }

  makeSum(arr) {
    // calculating a numeric value for each 'arr'(char)
    // by adding 1 to it's index and pushing that into
    // a new Array 'nArr'
    const nArr = [];
    arr.forEach(elem => {
        let idx = alphas.indexOf(elem) + 1;
        nArr.push(idx);
    });

    // Calculating a Sum of all elements of 'nArr'
    // then returning the Total Sum
    let sum = 0;
    nArr.forEach(elem => {
        sum += elem;
    });
    return sum;
  }

}

//  ==========  EXPORTING MODULE  ==========--:-- >>>
exports.Crypta = Crypta;
exports.HashIt = HashIt;

/////***********  CODE-END  ***********/////


// --------  Example Instantiation  -------- // */
//*****  !!!  Un-comment the code below to Test Crypta JS  !!!  *****//


// CRYPTA DEMO: //
// Below is some Demo text Data
/*
let Data = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id semper risus in hendrerit gravida rutrum quisque non. Scelerisque viverra mauris in aliquam sem. Nunc scelerisque viverra mauris in aliquam sem fringilla ut. Est sit amet facilisis magna etiam tempor orci. Risus nec feugiat in fermentum. Amet est placerat in egestas. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Non enim praesent elementum facilisis leo vel fringilla. Risus sed vulputate odio ut enim blandit volutpat maecenas. Ultrices eros in cursus turpis massa tincidunt dui ut ornare. Aliquam ut porttitor leo a. Arcu odio ut sem nulla pharetra diam sit amet.
`
const pswrd = 'xN*_*JfF91zR3-!69';
const cryptor = new Crypta(pswrd);
cryptor.cycles = 3; // Number of times to cycle encryption, higher number slower but more secure
let encrData = cryptor.Encrypt(Data);
console.log(`Encrypted Data:  '${encrData}'`);
let decrData = cryptor.Decrypt(encrData);
console.log(`\nDecrypted Data:  '${decrData}'`);
*/

//*****  !!!  Un-comment the code below to Test HashIt JS  !!!  *****//
/*
// HASHIT CLASS DEMO: //
const pswrd = 'xNJfF91zR3-!69';
const h = new HashIt(pswrd);
console.log(`Hash for '${pswrd}': ${h.hash}`);
console.log(`Length of Hash: ${h.hash.length}`);
*/
