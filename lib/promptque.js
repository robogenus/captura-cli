/*//// Module Introspection Stuff - Begin ////*/
const __MODULEINFO__ = {
    package: 'lib',
    name: 'promptque',
    sub: 'promptque class definition',
    context: 'Promptque is a CLI class module for creating command-line interface menu prompt queries that utilize Inquirer JS and Chalk JS Javascript Modules. Note: Requires Inquirer.js and Chalk.js npm modules',
    version: '1.0.0',
    dependencies: ['inquirer.js','shell.js','colors.js'],
    author: 'Donovan Edwards',
    author_email: 'robogenus@gmail.com',
    repository: undefined,
    license: 'MIT',
    banner: `\n
    -----------------------------
    :      {1001}^ -,           :
    :     _________  :  ___     :
    :    |       _=_ |     |    :
    :    |     K[*_^]3 (]  |    :
    :    | #    <=.=>  /   |    :
    :    | W)__/]888[\\/    |    :
    :    |     <| + |>     |    :
    :    |     ((<?>))     |    :
    :    |   |.]-DCE-[.|   |    :
    :    |   +[o]---[o]+   |    :
    :    |   +:::+ +:::+   |    :
    :    |  {[RoBoGeNus]]} |    :
    :    -------------------    :
    :                 © 2019    :
    -----------------------------
    `
}

/*//// Module Introspection Stuff - End ////*/
//------------------------------------------------------

/////***********  CODE-START  ***********/////

/* See Examples Below To Test This Class */

const inquirer = require('inquirer');
const sh = require('shelljs');
const colors = require('colors');

// class to instantiate an inquirer prompt
class PromptQue {
  constructor(params) {
    this.questions = params; // params is a passed Object as arguments in Object {key:value pairs} format
    this.answers = undefined; // a class property to hold user's answer to prompt
  }

  doPrompts() {
    return inquirer.prompt(this.questions);
  }

  que = async () => {
    this.answers = await this.doPrompts();
    //await console.log(this.answers);
    return await this.answers;
  }
}

//// Export Module:
module.exports = PromptQue;
exports.MODULEINFO = __MODULEINFO__;

/////***********  CODE-END  ***********/////

//----------------------------------------------------
// Example Usage:  (uncomment to run example code) >>>>

// //Example One:
// let qData = {
//   type: 'list',
//   name: 'pizza',
//   choices: ['Jumbo','Large','Standard','Medium','Small'],
//   message: 'What size Pizza would you like to order?',
//
// };
// const Prompt1 = new PromptQue(qData);
// //Prompt1.doPrompts();
// const getResponse = async () => {
//   const answers = await Prompt1.que();
//   //console.log(answers);
//   const choice = await answers.pizza;
//
//   console.log('\nYou have decided to order a '.brightCyan + choice.yellow + ' pizza\n'.brightCyan);
//   //return await answers;
// }
// getResponse();

// //Example Two:
// let qData = [
//   {
//     type: 'input',
//     name: 'fname',
//     message: 'What is your first name?'
//   },
//   {
//     type: 'input',
//     name: 'lname',
//     message: 'What is your last name?'
//   }
// ];
// const Prompt2 = new PromptQue(qData);
// //Prompt1.doPrompts();
// const getResponse = async () => {
//   const answers = await Prompt2.que();
//   //console.log(answers);
//   const { fname, lname } = await answers;
//   const fullname = `${fname} ${lname}`;
//
//   console.log('\nHello '.brightCyan + fullname.yellow + '. Nice to meet you!\n'.brightCyan);
//   //return await answers;
// }
// getResponse();

// -----------------------------------------------------
//-----------------------------------------------------

/* *****  DEV SAMPLE CODE  ***** /*
+-----
Put sample-test code below here then move
to ./devnotes/dev-test-code.<number>.txt
as needed.
+-----



*/
