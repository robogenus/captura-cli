
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('What is your name? ', (answer) => {
  if(answer === undefined) {
    console.log(`Answer: undefined`);
  } else if (answer === '') {
    console.log(`Answer: none`);
  } else {
    console.log(`Answer: ${answer}`);
  }

  rl.close();
});



// function que(question) {
//   let stdin = process.stdin,
//       stdout = process.stdout;
//
//   // stdin.resume();
//   stdout.write(question);
//
//   stdin.once('data', (data) => {
//         // callback(data.toString().trim());
//         return data.toString().trim();
//   });
// }
//
// let ans = que('What is your name? >> ');
//
// console.log(ans);
